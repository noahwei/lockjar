package com.hedian.lockjar;

import io.xjar.XConstants;
import io.xjar.XKit;
import io.xjar.boot.XBoot;
import io.xjar.key.XKey;

import javax.swing.*;
import java.io.File;
import java.util.Scanner;

/**
 * @program: lockjar
 * @description: 加密jar包工具类
 * @author: noahw
 * @create: 2019-11-26 15:34
 */
public class LockJarTool {

    public static void main(String[] args){
        String newFile = "";
        try {
            //准备密码和密钥
            String password = "coder@hedian";
            XKey xKey = XKit.key(password);

            System.out.println("请选择需要加密的jar包,输入y回车继续,输入其他按回车退出");
            String input = "";
            Scanner s = new Scanner(System.in);
            String filePath = "";
            while(true){
                input = s.nextLine().trim();
                if("y".equals(input)){
                    JFileChooser fd = new JFileChooser();
                    fd.setFileSelectionMode(JFileChooser.OPEN_DIALOG);
                    fd.showOpenDialog(null);
                    File f = fd.getSelectedFile();
                    filePath = f.getPath();
                    break;
                }
                else if("n".equals(input)){
                    System.out.println("请关闭窗口");
                    System.exit(0);
                }
            }


            //获取文件名和路径名
            String[] fileArr = filePath.split("\\\\");
            String fileName = fileArr[fileArr.length - 1];
            String pathName = filePath.split(fileName)[0];
            System.out.println(pathName);
            if(!fileName.endsWith(".jar")){
                System.out.println("选择的文件为非jar包,请关闭程序重新选择");
            }
            System.out.println("正在加密...,请不要退出");

            String tempFileName = System.currentTimeMillis() + "_temp.jar";
            newFile = pathName + tempFileName;
            XBoot.encrypt(filePath, newFile, xKey, XConstants.MODE_DANGER);

            File file1 = new File(filePath);
            if (!file1.isDirectory()) {
                file1.delete();
            }

            File file2 = new File(newFile);
            file2.renameTo(file1);
            System.out.println("加密完成");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            File file = new File(newFile);
            if(file.exists()){
                file.delete();
            }
        }
    }
}